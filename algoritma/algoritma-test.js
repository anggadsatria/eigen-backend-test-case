// 1.
function reverseString(string) {
    if(typeof string != 'string') return ('Parameter harus berupa string')
    if(string.length < 1 || string == ' ') return ('Parameter tidak boleh kosong')

    let result = ''
    let tempNumber = ''

    for (let index = string.length-1; index >= 0; index--) {
        let tempString = string.charAt(index)
        if(!isNaN(tempString)) {
            tempNumber += string.charAt(index)
        }
        else {
            result += string.charAt(index)
        }
    }

    for (let index = tempNumber.length-1; index >= 0; index--) {
        result += tempNumber.charAt(index)
    }

    return result
}
console.log(reverseString("EIGEN1"))
// console.log(reverseString("EIGEN2134"))
// console.log(reverseString(1))
// console.log(reverseString(''))

// 2.
function longestSentence(string) {
    if(typeof string != 'string') return ('Parameter harus berupa string')
    if(string.length < 1 || string == ' ') return ('Parameter tidak boleh kosong')

    let arrayString = ['']
    let tempIndex = 0
    let longestString = ''
    let longestCharLength = 0

    // Split string
    for (let index = 0; index < string.length; index++) {
        if(string.charAt(index) === ' ') {
            tempIndex++
            arrayString[arrayString.length] = ''
        } else {
            arrayString[tempIndex] += string.charAt(index)
        }   
    }

    // Count the highest number of sentence length
    for (let index = 0; index < arrayString.length; index++) {
        if(arrayString[index].length > longestCharLength) {
            longestCharLength = arrayString[index].length
            longestString = arrayString[index]
        }
    }
    
    let result = `${longestString}: ${longestCharLength} character`
    return result

}
console.log(longestSentence("Saya sangat senang mengerjakan soal algoritma"))
// console.log(longestSentence(1))
// console.log(longestSentence(''))

// 3. 
function stringSearch(input, query) {
    if(typeof input !== 'object' || typeof query != 'object' ) return('Input atau query harus berupa array')
    if(input.length < 1 || query.length < 1) return('Input atau query tidak boleh kosong')

    let countQuery = [0]
    let tempIndex = 0

    for (let i = 0; i < query.length; i++) {
        let tempNumber = 0
        for (let j = 0; j < input.length; j++) {
            if(query[i] === input[j]) {
                tempNumber++
            }
            countQuery[tempIndex] = tempNumber
        }        
        tempIndex++
    }

    const result = `OUTPUT = ${countQuery}`
    return result
}

let INPUT = ['xc', 'dz', 'bbb', 'dz']  
let QUERY = ['bbb', 'ac', 'dz']   
console.log(stringSearch(INPUT, QUERY));

// 4.
function countDiagonalMatrix(matrix) {
    const matrixLength = matrix.length
    let countMatrix = [0,0]

    for (let i = 0; i < matrixLength; i++) {
        countMatrix[0] += matrix[i][i]
        countMatrix[1] += matrix[i][matrixLength - i - 1]
    }
    const result = `maka hasilnya adalah ${countMatrix[0]} - ${countMatrix[1]} = ${countMatrix[0] - countMatrix[1]}`
    return result
}

let matrix = [[1, 2, 0], [4, 5, 6], [7, 8, 9]]
console.log(countDiagonalMatrix(matrix));