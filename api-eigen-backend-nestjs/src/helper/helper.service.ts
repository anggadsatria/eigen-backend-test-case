import { Injectable } from '@nestjs/common';

@Injectable()
export class HelperService {
    constructor() {}

    generateUniqueCode(title) {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        const digits = '0123456789';
        
        let countWhiteSpace = (title.split(" ").length - 1);

        // Generate a random two-digit number
        const randomNumber = digits[Math.floor(Math.random() * digits.length)] + digits[Math.floor(Math.random() * digits.length)];

        if(countWhiteSpace < 2) {
            const randomChar1 = characters[Math.floor(Math.random() * characters.length)];
            const randomChar2 = characters[Math.floor(Math.random() * characters.length)];
            const code = `${randomChar1}${randomChar2}-${randomNumber}`;
            return code;
        } 
          
        const randomChar1 = characters[Math.floor(Math.random() * characters.length)];
        const randomChar2 = characters[Math.floor(Math.random() * characters.length)];
        const randomChar3 = characters[Math.floor(Math.random() * characters.length)];
        const code = `${randomChar1}${randomChar2}${randomChar3}-${randomNumber}`;
        return code;

    }
}