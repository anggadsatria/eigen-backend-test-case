import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { BadRequestException, ValidationPipe, VersioningType } from '@nestjs/common';
import { useContainer } from 'class-validator';
import { HttpExceptionFilter } from './filter/http-exception.filter';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  const globalPrefix = 'api';

  app.setGlobalPrefix(globalPrefix);

  app.enableVersioning({
    type: VersioningType.URI,
    defaultVersion: process.env.API_VERSION,
  });


  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      exceptionFactory: (errors) => {
        const result = errors.map((error) => {
          //#region Menangani kesalahan validasi dari properti bersarang
          if (error.children?.length > 0) {
            const nestedErrors = error.children.map((childError) => {
              const getChildren = Object.values(childError.children)[0]; // children validation data
              if (!getChildren) {
                const getChildren = childError;
                const message = Object.values(getChildren.constraints)[0];
                const parentProperty = error.property; // parent property names
                return `${parentProperty}: ${message}`;
              }
              const parentProperty = error.property; // parent property names
              const message = Object.values(getChildren.constraints)[0]; // messages

              return `${parentProperty}: ${message}`; // merge property with error messages
            });

            return nestedErrors.join(', '); // Menggabungkan pesan kesalahan dari properti bersarang menjadi satu string
          }
          //endregion

          //#region handle validation error from parrents
          return error.constraints[Object.keys(error.constraints)[0]];
          //#endregion
        });
        throw new BadRequestException(`${result}`);
      },
      stopAtFirstError: true,
    }),
  );

  // enable DI for class-validator
  // this is an important step, for further steps in this article
  useContainer(app.select(AppModule), { fallbackOnErrors: true });

  app.useGlobalFilters(new HttpExceptionFilter());

  const config = new DocumentBuilder()
    .setTitle('Eigen Backend Test Case API')
    .setDescription('Dokumentasi API untuk Eigen Backend Test Case API')
    .setVersion('1.0')
    .addTag('API')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger', app, document);
  
  await app.listen(3000);

}
bootstrap();
