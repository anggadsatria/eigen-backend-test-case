import { Module } from '@nestjs/common';
import { BookModule } from './controller/book/book.module';
import { MemberModule } from './controller/member/member.module';
import { BorrowBookModule } from './controller/borrow-book/borrow-book.module';
import { APP_GUARD, RouterModule } from '@nestjs/core';
import { AuthModule } from './auth/auth.module';
import { UserController } from './controller/user/user.controller';
import { UserModule } from './controller/user/user.module';
import { AuthGuard } from './auth/auth.guard';

@Module({
  imports: [
    BookModule,
    MemberModule,
    BorrowBookModule,
    AuthModule,
    UserModule,
    RouterModule.register([
      {
        path: 'member',
        module: MemberModule
      },
      {
        path: 'book',
        module: BookModule
      },
      {
        path: 'borrow-book',
        module: BorrowBookModule
      },
      {
        path: 'user',
        module: UserModule
      },
      {
        path: 'auth',
        module: AuthModule
      }
    ]),
  
  ],
  controllers: [],
  providers: [
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
})
export class AppModule {}
