export function createErrorResponse(res, req, error) {
    return res.status(error.status).json({
      statusCode: error.status,
      message: error['response']['message'],
      timestamp: new Date().toISOString(),
      path: req.url,
    });
  }
  
  export function createInternalErrorResponse(res, error) {
    return res.status(500).json({
      code: error.status,
      message: error.message,
      timestamp: new Date().toISOString(),
      path: res.url,
    });
  }
  