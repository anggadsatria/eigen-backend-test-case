import { IsNotEmpty, IsString, IsInt, Min } from 'class-validator';

export class CreateBookDTO {
  //   @IsNotEmpty()
  //   @IsString()
  code: string;

  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  author: string;

  @IsNotEmpty()
  @IsInt()
  @Min(1)
  stock: number;
}