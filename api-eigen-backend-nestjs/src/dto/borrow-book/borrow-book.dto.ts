import { IsNotEmpty, IsString, IsInt, Min } from 'class-validator';

export class CreateBorrowedBookDTO {

    @IsNotEmpty()
    @IsString()
    memberId: string;
    
    @IsNotEmpty()
    @IsString()
    bookId: string;
}