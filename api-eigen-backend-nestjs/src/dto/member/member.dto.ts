import { IsNotEmpty, IsString, IsInt, Min } from 'class-validator';

export class CreateMemberDTO {
  code: string;

  @IsNotEmpty()
  @IsString()
  name: string;

  quota: number;

  isPenalized: boolean;
}