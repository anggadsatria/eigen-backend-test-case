import { Injectable } from '@nestjs/common';
import { BorrowedBook } from '@prisma/client';
import { CreateBorrowedBookDTO } from 'src/dto/borrow-book/borrow-book.dto';
import { CreateMemberDTO } from 'src/dto/member/member.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class BorrowBookService {
    constructor(private prisma: PrismaService) {}

    async index(): Promise<BorrowedBook[]> {
        const result = await this.prisma.borrowedBook.findMany()

        return result
    }

    async create(data: CreateBorrowedBookDTO) {
        const borrowBook = this.prisma.borrowedBook.create({
            data: {
                ...data,
            }
        })


        const updateBook = this.prisma.book.update({
            where: {
                id: data.bookId,
            },
            data: {
                stock: {
                    decrement: 1
                }
            },
        })

        const updateMember = this.prisma.member.update({
            where: {
                id: data.memberId,
            },
            data: {
                quota: {
                    increment: 1
                }
            },
        })

        const memberBorrowBook = await this.prisma.$transaction([
            borrowBook,
            updateBook,  
            updateMember
        ]);

        return {
            ...memberBorrowBook[0],
            books: memberBorrowBook[1],
            borrowers: memberBorrowBook[2]
        }
    }

    async returnBook(data: CreateBorrowedBookDTO) {
        const borrowBook = this.prisma.borrowedBook.create({
            data: {
                ...data,
            }
        })

        const updateBook = this.prisma.book.update({
            where: {
                id: data.bookId,
            },
            data: {
                stock: {
                    decrement: 1
                }
            },
        })

        const updateMember = this.prisma.member.update({
            where: {
                id: data.memberId,
            },
            data: {
                quota: {
                    increment: 1
                }
            },
        })

        const memberBorrowBook = await this.prisma.$transaction([
            borrowBook,
            updateBook,  
            updateMember
        ]);

        return {
            ...memberBorrowBook[0],
            books: memberBorrowBook[1],
            borrowers: memberBorrowBook[2]
        }
    }
}
