import { Controller, Req, Res, Get, Post, Put, Param, Delete, Body, ValidationPipe, UnprocessableEntityException} from '@nestjs/common';
import { Request, Response } from 'express';
import { BookService } from '../book/book.service';
import { HelperService } from 'src/helper/helper.service';
import { MemberService } from '../member/member.service';
import { CreateBorrowedBookDTO } from 'src/dto/borrow-book/borrow-book.dto';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';
import { createErrorResponse } from 'src/filter/error.filter';
import { BorrowBookService } from './borrow-book.service';

@ApiTags('Book')
@ApiBearerAuth()
@Controller({ version: '1' })
export class BorrowBookController {
    constructor(
        private readonly bookService: BookService,
        private readonly memberService: MemberService,
        private readonly helperService: HelperService,
        private readonly borrowBookService: BorrowBookService,
    ) {}

    @Post()
    @ApiBody({
        type: CreateBorrowedBookDTO, // Specify the DTO class representing the request body
        description: 'Create Borrowed Book', // Description of the request body
    })
    async create(
        @Body(ValidationPipe) body: CreateBorrowedBookDTO,
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            const bookData = await this.bookService.findById(body.bookId)
            if(!bookData) throw new UnprocessableEntityException('Sorry, the book has been borrowed')

            const memberData = await this.memberService.findById(body.memberId)
            if(memberData.isPenalized === true)  throw new UnprocessableEntityException('The member has been penalized')
            if(memberData.quota === 2) throw new UnprocessableEntityException('Member cannot borrowMembers may not borrow more than 2 books')


            const newData = new CreateBorrowedBookDTO();
            newData.bookId = body.bookId
            newData.memberId = body.memberId
            
            const result = await this.borrowBookService.create(newData)
    
            return response.status(200).json({
                data: result
              });
        } catch (error) {
            console.log(error);
            return createErrorResponse(response, request,error);
        }
    }
}
