import { Module } from '@nestjs/common';
import { BorrowBookService } from './borrow-book.service';
import { BorrowBookController } from './borrow-book.controller';
import { BookService } from '../book/book.service';
import { MemberService } from '../member/member.service';
import { PrismaService } from 'src/prisma.service';
import { HelperService } from 'src/helper/helper.service';

@Module({
  providers: [BorrowBookService, BookService, MemberService, PrismaService, HelperService],
  controllers: [BorrowBookController]
})
export class BorrowBookModule {}
