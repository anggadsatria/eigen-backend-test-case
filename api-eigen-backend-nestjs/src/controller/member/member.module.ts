import { Module } from '@nestjs/common';
import { MemberService } from './member.service';
import { PrismaService } from 'src/prisma.service';
import { HelperService } from 'src/helper/helper.service';
import { MemberController } from './member.controller';

@Module({
  controllers: [MemberController],
  providers: [MemberService, PrismaService, HelperService]
})
export class MemberModule {}
