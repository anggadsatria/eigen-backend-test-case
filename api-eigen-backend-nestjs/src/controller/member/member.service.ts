import { Injectable } from '@nestjs/common';
import { Member } from '@prisma/client';
import { CreateMemberDTO } from 'src/dto/member/member.dto';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class MemberService {
    constructor(private prisma: PrismaService) {}

    async index(): Promise<Member[]> {
        const result = await this.prisma.member.findMany()

        return result
    }

    async find(code: string): Promise<Member> {
        const result = await this.prisma.member.findUnique({
            where: {
                code: code
            },
            include: {
                borrowedBooks: {
                    select: {
                        id: true,
                        bookId: true,
                        isReturn: true,
                        books: {
                            select: {
                                code: true,
                                title: true,
                                author: true,
                                stock: true
                            }
                        }
                    },

                }
            }
        })

        return result
    }

    async findById(id: string): Promise<Member> {
        const result = await this.prisma.member.findUnique({
            where: {
                id: id
            }
        })

        return result
    }

    async codeIsExist(code: string): Promise<boolean> {
        const result = await this.prisma.member.findUnique({
            where: {
                code: code
            }
        })

        return result ? true : false
    }

    async create(data: CreateMemberDTO): Promise<Member> {
        const result = await this.prisma.member.create({
            data: {
                ...data,
            }
        })

        return result
    }
}
