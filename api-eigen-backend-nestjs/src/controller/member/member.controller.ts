import { Controller, Req, Res, Get, Post, Put, Param, Delete, Body, ValidationPipe, UnprocessableEntityException} from '@nestjs/common';
import { Request, Response } from 'express';
import { MemberService } from './member.service';
import { HelperService } from 'src/helper/helper.service';
import { createErrorResponse } from 'src/filter/error.filter';
import { CreateMemberDTO } from 'src/dto/member/member.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('Member')
@ApiBearerAuth()
@Controller({ version: '1' })
export class MemberController {
    constructor(
        private readonly memberService: MemberService,
        private readonly helperService: HelperService
    ) {}

    @Get()
    async index(
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            const result = await this.memberService.index()
            return response.status(200).json({
                data: result
              });
        } catch (error) {
            console.log(error);
            
            return createErrorResponse(
                response,
                request,
                'Request untuk mencari semua data tidak dapat diproses',
              );
        }
    }

    @Get(':code')
    async find(
        @Param('code') code: string,
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
         
            const result = await this.memberService.find(code)
            return response.status(200).json({
                data: result
            });
        } catch (error) {
            console.log(error);
            
            return createErrorResponse(
                response,
                request,
                'Request untuk mencari semua data tidak dapat diproses',
              );
        }
    }

    @Post()
    async create(
        @Body(ValidationPipe) body: CreateMemberDTO,
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            const generateCode = await this.helperService.generateUniqueCode(body.name)
            
            const codeIsExist = await this.memberService.codeIsExist(generateCode)
            if(codeIsExist) throw new UnprocessableEntityException('Member code is being used')
            
            const newData = new CreateMemberDTO()
            newData.code = generateCode
            newData.name = body.name
            newData.quota = 0

            const result = await this.memberService.create(newData)
            return response.status(201).json({
                data: result
            });
        } catch (error) {
            console.log(error);
            
            return createErrorResponse(
                response,
                request,
                'Request untuk mencari semua data tidak dapat diproses',
              );
        }
    }
}
