import { Module } from '@nestjs/common';
import { BookController } from './book.controller';
import { PrismaService } from 'src/prisma.service';
import { BookService } from './book.service';
import { HelperService } from 'src/helper/helper.service';

@Module({
    controllers: [BookController],
    providers: [PrismaService, BookService, HelperService]
})
export class BookModule {}
