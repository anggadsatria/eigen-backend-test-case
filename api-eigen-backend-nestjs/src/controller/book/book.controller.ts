import { Controller, Req, Res, Get, Post, Put, Param, Delete, Body, ValidationPipe, UnprocessableEntityException} from '@nestjs/common';
import { Request, Response } from 'express';
import { BookService } from './book.service';
import { createErrorResponse } from 'src/filter/error.filter';
import { HelperService } from 'src/helper/helper.service';
import { CreateBookDTO } from 'src/dto/book/book.dto';
import { ApiBearerAuth, ApiBody, ApiTags } from '@nestjs/swagger';


@ApiTags('Book')
@ApiBearerAuth()
@Controller({ version: '1' })
export class BookController {
    constructor(
        private readonly bookService: BookService,
        private readonly helperService: HelperService
    ) {}

    @Get()
    async index(
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            const result = await this.bookService.index()
            return response.status(200).json({
                data: result
              });
        } catch (error) {
            console.log(error);
            
            return createErrorResponse(
                response,
                request,
                'Request untuk mencari semua data tidak dapat diproses',
              );
        }
    }

    @Post()
    @ApiBody({
        type: CreateBookDTO, // Specify the DTO class representing the request body
        description: 'Create Book', // Description of the request body
      })
    async create(
        @Body(ValidationPipe) body: CreateBookDTO,
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            const generateCode = await this.helperService.generateUniqueCode(body.title)

            const codeIsExist = await this.bookService.codeIsExist(generateCode)
            if(codeIsExist) throw new UnprocessableEntityException('')

            const newData = new CreateBookDTO();
            newData.code = generateCode
            newData.author = body.author
            newData.title = body.title
            newData.stock = body.stock

            const result = await this.bookService.create(newData)
    
            return response.status(200).json({
                data: result
              });
        } catch (error) {
            console.log(error);
            return createErrorResponse(response, request,error);
        }
    }

    @Put(':code')
    @ApiBody({
        type: CreateBookDTO, // Specify the DTO class representing the request body
        description: 'Update Book', // Description of the request body
    })
    async update(
        @Param('code') code: string,
        @Body(ValidationPipe) body: CreateBookDTO,
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            console.log(code);
            
            const codeIsExist = await this.bookService.codeIsExist(code)
            if(!codeIsExist) throw new UnprocessableEntityException('Kode tidak ditemukan')

            const newData = new CreateBookDTO();
            newData.author = body.author
            newData.title = body.title
            newData.stock = body.stock

            const result = await this.bookService.update(code, newData)
    
            return response.status(200).json({
                data: result
            });
        } catch (error) {
            console.log(error);
            return createErrorResponse(response, request,error);
        }
    }

    @Delete(':code')
    @ApiBody({
        type: CreateBookDTO, // Specify the DTO class representing the request body
        description: 'Update Book', // Description of the request body
      })
    async destroy(
        @Param('code') code: string,
        @Req() request: Request,
        @Res() response: Response,
    ){
        try {
            console.log(code);
            
            const codeIsExist = await this.bookService.codeIsExist(code)
            if(!codeIsExist) throw new UnprocessableEntityException('Kode tidak ditemukan')

            const result = await this.bookService.destroy(code)
    
            return response.status(204);
        } catch (error) {
            console.log(error);
            return createErrorResponse(response, request,error);
        }
    }
}
