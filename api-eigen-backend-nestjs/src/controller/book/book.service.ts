import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { Book } from '@prisma/client';
import { CreateBookDTO } from 'src/dto/book/book.dto';


@Injectable()
export class BookService {
    constructor(private prisma: PrismaService) {}

    async index(): Promise<Book[]> {
        const findAll = await this.prisma.book.findMany()

        return findAll
    }

    async find(title: string): Promise<Book> {
        const findAll = await this.prisma.book.findFirst({
            where: {
                title: title
            }
        })

        return findAll
    }

    async findById(id: string): Promise<Book> {
        const findAll = await this.prisma.book.findUnique({
            where: {
                id: id,
                stock: {
                    not: 0
                }
            }
        })

        return findAll
    }

    async codeIsExist(code: string): Promise<boolean> {
        const result = await this.prisma.book.findUnique({
            where: {
                code: code
            }
        })

        return result ? true : false
    }

    async create(data: CreateBookDTO): Promise<Book> {
        const result = await this.prisma.book.create({
            data: {
                ...data,
            }
        })

        return result
    }

    async update(code: string, newData: CreateBookDTO): Promise<Book> {
        const result = await this.prisma.book.update({
            where: {
                code: code,
            },
            data: {
                ...newData
            }
        })

        return result
    }

    async destroy(code: string): Promise<void> {
        const result = await this.prisma.book.delete({
            where: {
                code: code,
            },
        })
    }
}
