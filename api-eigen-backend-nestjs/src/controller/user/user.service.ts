import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';

@Injectable()
export class UserService {
    constructor(private prisma: PrismaService) {}
    
    async find(username: string) {
        const result = this.prisma.user.findUnique({
          where: {
            username: username,
          }
        })
  
        return result
      }
}
