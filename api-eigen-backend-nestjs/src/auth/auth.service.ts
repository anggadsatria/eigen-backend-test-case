import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/controller/user/user.service';
const bcrypt = require('bcrypt');

@Injectable()
export class AuthService {
    constructor(
        private usersService: UserService,
        private jwtService: JwtService
    ) {}

    async signIn(
        username: string,
        password: string,
    ): Promise<{ access_token: string }> {
    const user = await this.usersService.find(username);
        
    if(!user) throw new UnauthorizedException('User not found')

    const passwordMatch = await bcrypt.compare(password, user.password);  
    
    if(!passwordMatch) throw new UnauthorizedException('Wrong Password')
    console.log(user);
    
    const payload = { sub: user.id, username: user.username, role: user.role };
    return {
        access_token: await this.jwtService.signAsync(payload),
    };
    }
}
