import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private reflector: Reflector) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    // get the roles required
    const requiredRoles = this.reflector.getAllAndOverride<string[]>(
      'ROLES_KEY',
      [context.getHandler(), context.getClass()],
    );
    if (!requiredRoles) {
      return false;
    }
    const { user } = context.switchToHttp().getRequest();
    const userRoles = user['role_pintu_notaris'];
    console.log(userRoles);
    

    const result = this.validateRoles(requiredRoles, userRoles);
    if (!result) throw new ForbiddenException('Anda tidak memiliki hak akses');
    return this.validateRoles(requiredRoles, userRoles);
  }

  validateRoles(roles: string[], userRoles: string[]) {
    return roles.some((role) => userRoles.includes(role));
  }
}
