export const config = {
  MINIO_ENDPOINT: process.env.MINIO_ENDPOINT,
  MINIO_PORT: 9000,
  MINIO_ACCESSKEY: process.env.MINIO_ACCESS_KEY,
  MINIO_SECRETKEY: process.env.MINIO_SECRET_KEY,
  MINIO_BUCKET: process.env.MINIO_BUCKET,
  MINIO_USE_SSL: process.env.MINIO_SSL === 'true' ? true : false,
};
