export enum Role {
  ADMIN = 'Admin',
}

export enum RoleStatus {
  AKTIF = 'Aktif',
  NON_AKTIF = 'Non Aktif'
}