import { PrismaClient } from '@prisma/client'
const bcrypt = require('bcrypt');

const prisma = new PrismaClient()
async function main() {
  const hash = await bcrypt.hash('secret1234', 10)  
  const admin1 = await prisma.user.upsert({
    where: { username: 'admin@prisma.io' },
    update: {},
    create: {
      username: 'admin.eigen@prisma.io',
      name: 'Siska',
      password: hash,
      role: 'Admin'
    },
  })
  console.log({ admin1 })
}
main()
  .then(async () => {
    await prisma.$disconnect()
  })
  .catch(async (e) => {
    console.error(e)
    await prisma.$disconnect()
    process.exit(1)
  })